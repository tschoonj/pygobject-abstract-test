import gi
from gi.repository import Pygabstract

import unittest

class Implemented(Pygabstract.Simple):
    def do_get_name(self):
        return "Implemented"

class Test(unittest.TestCase):
    def test_python_class(self):
        instance = Implemented()
        self.assertEqual(instance.get_name(), "Implemented")
        self.assertEqual(instance.get_name(), "Implemented")

    def test_c_class(self):
        instance = Pygabstract.Implc()
        self.assertEqual(instance.get_name(), "PygabstractImplc")
        self.assertEqual(instance.get_name(), "PygabstractImplc")

if __name__ == '__main__':
    unittest.main(verbosity=2)
