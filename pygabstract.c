#include "pygabstract.h"

G_DEFINE_ABSTRACT_TYPE(PygabstractSimple, pygabstract_simple, G_TYPE_OBJECT)

static const gchar* pygabstract_simple_real_get_name(PygabstractSimple *self) {
	return "PygabstractSimple";
}

static void pygabstract_simple_init(PygabstractSimple *self) {

}

static void pygabstract_simple_class_init(PygabstractSimpleClass *klass) {
	klass->get_name = pygabstract_simple_real_get_name;
}

const gchar* pygabstract_simple_get_name(PygabstractSimple *self) {
	return PYGABSTRACT_SIMPLE_GET_CLASS(self)->get_name(self);
}

struct _PygabstractImplc {
	PygabstractSimple parent_instance;
};

G_DEFINE_TYPE(PygabstractImplc, pygabstract_implc, PYGABSTRACT_TYPE_SIMPLE)

static void pygabstract_implc_init(PygabstractImplc *self) {

}

static const gchar* pygabstract_implc_real_get_name(PygabstractSimple *self) {
	return "PygabstractImplc";
}

static void pygabstract_implc_class_init(PygabstractImplcClass *klass) {
	PygabstractSimpleClass *base_klass = PYGABSTRACT_SIMPLE_CLASS(klass);
	base_klass->get_name = pygabstract_implc_real_get_name;
}



