#include <glib-object.h>

#ifndef PYGABSTRACT_H
#define PYGABSTRACT_H

#define PYGABSTRACT_TYPE_SIMPLE pygabstract_simple_get_type()
G_DECLARE_DERIVABLE_TYPE(PygabstractSimple, pygabstract_simple, PYGABSTRACT, SIMPLE, GObject)

struct _PygabstractSimpleClass {
  GObjectClass parent_class;

  const gchar* (* get_name) (PygabstractSimple *instance);
};

const gchar* pygabstract_simple_get_name(PygabstractSimple *instance);

#define PYGABSTRACT_TYPE_IMPLC pygabstract_implc_get_type()
G_DECLARE_FINAL_TYPE(PygabstractImplc, pygabstract_implc, PYGABSTRACT, IMPLC, PygabstractSimple)

#endif
